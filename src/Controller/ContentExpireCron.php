<?php

/**
 * @file
 * Contains Drupal\content_expire\Controller\ContentExpireCron.
 */

namespace Drupal\content_expire\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity;
use Drupal\node\Entity\Node;
use Drupal\workflows_email\Controller\WorkflowsEmailController;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\group\Entity\GroupContentType;

/**
 * Class idbCron.
 *  @package Drupal\content_expire\Controller
 */
class ContentExpireCron extends ControllerBase {

  /**
   * Custom function for inform expiry date to author.
   */
  public static function notifyExpiry() {
    $config = \Drupal::config('content_expiry.settings');
    $message['subject'] = $config->get('content_expiry.expiry_email_subject');
    $message['content'] = $config->get('content_expiry.expiry_notification_email_template');
    $frequency = $config->get('content_expiry.expiry_email_frequency');
    // Initialize obj of Email Composer.
    $message_notification = new \Drupal\email_compose\Controller\EmailComposeController();
    // @To Do - Change content types as a programmatically.
    $contents = ['news', 'events', 'blog'];
    $frequencies = explode(",", $frequency);
    foreach ($frequencies as $frequency) {
      if (is_numeric($frequency) && $frequency >=0 ) {  //Check frequency is numeric and valid.
        // Load expiry content Ids.
        $nids = self::getExpiryContents($frequency);
        // Load expiry contents using nodeIds.
        $datas = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);
        if (!empty($datas)) {
          foreach ($datas as $data) {
            $email_cc_dept_heads = '';
            $email_cc_dept_heads = self::getDeptheads($data); // Get department head and content cordinator.
            $url = $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $data->id());
            $title = $alias = $data->getTitle();
            $type = $alias = ucwords($data->getType());
            $email = $data->getOwner()->get('mail')->value;
            $author = $data->getOwner()->get('name')->value;
            $expiry_date = $data->get('field_expiry_date')->value;
            $tokens['token:url'] = $url;
            $tokens['token:title'] = $title;
            $tokens['token:email'] = $email;
            $tokens['token:author'] = $author;
            $tokens['token:expiry_date'] = $expiry_date;
            $tokens['token:type'] = $type;
            // Send Notification.
            $workflows_email = new WorkflowsEmailController();
            $content['to'] = $email . $email_cc_dept_heads;
            $content['subject'] = WorkflowsEmailController::contentTokenReplacement($message['subject'], $tokens);
            $content['message'] = WorkflowsEmailController::contentTokenReplacement($message['content'], $tokens);
            $mail_send = $message_notification->emailComposeSendEmail($content);
          }
        }
      }
    }
    return t('Cron updated successfully');
  }

  /**
   * Custom function get all node ids based on the type.
   *
   * @Return Node Ids
   */
  public static function getExpiryContents($frequency) {
    $entity_ids = [];
    $current_date = date('Y-m-d', strtotime('+'.$frequency.' day'));
    $query = \Drupal::entityQuery('node');
      //$query->condition('type', $type);
      $query->condition('field_expiry_date.value', $current_date, '=');
      $entity_ids = $query->execute();
    if (!empty($entity_ids)) {
      return $entity_ids;
    }
    else {
      return $entity_ids;
    }
  }

  /**
   * Custom function for closed coments of blog after expire date.
   */
  public function closeExpireComments() {
    $current_date = date('Y-m-d');
    $query = \Drupal::entityQuery('node');
      $query->condition('type', 'blog');
      $query->condition('field_expiry_date.value', $current_date, '<');
      $query->condition('field_comments.status', 1, '!=');
      $nodes = $query->execute();
    $comments = [];
    foreach ($nodes as $nid) {
      $node = Node::load($nid);
      if ($node->get('field_comments')->status != 1) {
        $node->get('field_comments')->status = CommentItemInterface::CLOSED;
        $node->save();
        $comments[] = $nid;
      }
    }
    if (!empty($comments)) {
      $message = t('Closed comments on following articles @nodes on' . $current_date , array('@nodes' => json_encode($comments)));
      \Drupal::logger('closed_comments')->notice($message);
    }
    return $nodes;
  }

  /**
   * Custom function to get Dept heads.
   */
  public function getDeptheads($node) {
    $email_cc = '';
    $plugin_id = 'group_node:' . $node->bundle(); // Only act if there are group content types for this node type.
    $group_content_types = GroupContentType::loadByContentPluginId($plugin_id);
    if (!empty($group_content_types)) {
      // Load all the group content for this node.
      $group_contents = \Drupal::entityTypeManager()
        ->getStorage('group_content')
        ->loadByProperties([
          'type' => array_keys($group_content_types),
          'entity_id' => $node->id(),
      ]);

      if(!empty($group_contents)) {
        /** @var \Drupal\group\Entity\GroupInterface[] $groups */
        $groups = [];
        foreach ($group_contents as $group_content) {
          /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
          $group = $group_content->getGroup();
          $groups_id = $group->id();
          $roles = ['department-content_coordinator', 'department-head'];
          foreach ($roles as $key => $role) {
            $members = $group->getMembers($role);
            if (!empty($members)) {
              foreach ($members as $member) {
                $users[] = $member->getUser()->getEmail();
              }
            }
          }
        }
      }
      $users = array_unique($users);
      foreach ($users as $user) {
        $email_cc .= "," . $user;
      }
    }
    return $email_cc;
  }
}

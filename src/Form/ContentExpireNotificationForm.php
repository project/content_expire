<?php

/**
 * @file
 * Contains Drupal\content_expire\ContentExpireNotificationForm
 */

namespace Drupal\content_expire\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

class ContentExpireNotificationForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_expiry_notification_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_expiry.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('content_expiry.settings');
    $form['expiry_email_subject'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Subject.'),
      '#default_value' => $config->get('content_expiry.expiry_email_subject'),
    );
    $form['expiry_notification_email_template'] = array(
      '#type' => 'text_format',
      '#required' => TRUE,
      '#title' => $this->t('Email Template for New contents.'),
      '#default_value' => $config->get('content_expiry.expiry_notification_email_template'),
    );
    $form['expiry_email_frequency'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Email Frequency'),
      '#description' => $this->t('Time Frequency seprated by comma'),
      '#default_value' => $config->get('content_expiry.expiry_email_frequency'),
    );
    $form['token_info'] = array(
      '#type' => 'markup',
      '#markup' => $this->t("Available tokens are : [token:url], [token:title], [token:email], [token:author], [token:expiry_date], [token:type]"),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('content_expiry.settings');
    $config->set('content_expiry.expiry_email_subject', $form_state->getValue('expiry_email_subject'));
    $config->set('content_expiry.expiry_notification_email_template', $form_state->getValue('expiry_notification_email_template')['value']);
    $config->set('content_expiry.expiry_email_frequency', $form_state->getValue('expiry_email_frequency'));
    $config->save();
    return parent::submitForm($form, $form_state);
   }
}
